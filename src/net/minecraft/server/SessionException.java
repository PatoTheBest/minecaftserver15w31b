package net.minecraft.server;

public class SessionException extends Exception {
   public SessionException(String var1) {
      super(var1);
   }
}
