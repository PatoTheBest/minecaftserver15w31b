package net.minecraft.server;

import net.minecraft.server.IDataManager;
import net.minecraft.server.IProgressUpdate;

public interface class_avq {
   IDataManager a(String var1, boolean var2);

   void d();

   boolean e(String var1);

   boolean b(String var1);

   boolean a(String var1, IProgressUpdate var2);
}
