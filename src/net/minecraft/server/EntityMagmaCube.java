package net.minecraft.server;

public class EntityMagmaCube extends EntitySlime {
   public EntityMagmaCube(World var1) {
      super(var1);
      this.fireProof = true;
   }

   protected void aY() {
      super.aY();
      this.a(class_wl.d).a(0.20000000298023224D);
   }

   public boolean cf() {
      return this.world.ab() != class_om.a;
   }

   public boolean cg() {
      return this.world.a((AxisAlignedBB)this.getBoundingBox(), (Entity)this) && this.world.getCubes((Entity)this, (AxisAlignedBB)this.getBoundingBox()).isEmpty() && !this.world.containsLiquid(this.getBoundingBox());
   }

   public int bs() {
      return this.cB() * 3;
   }

   public float c(float var1) {
      return 1.0F;
   }

   protected EnumParticle n() {
      return EnumParticle.A;
   }

   protected EntitySlime cu() {
      return new EntityMagmaCube(this.world);
   }

   protected Item D() {
      return Items.MAGMA_CREAM;
   }

   protected void b(boolean var1, int var2) {
      Item var3 = this.D();
      if(var3 != null && this.cB() > 1) {
         int var4 = this.random.nextInt(4) - 2;
         if(var2 > 0) {
            var4 += this.random.nextInt(var2 + 1);
         }

         for(int var5 = 0; var5 < var4; ++var5) {
            this.a(var3, 1);
         }
      }

   }

   public boolean av() {
      return false;
   }

   protected int cv() {
      return super.cv() * 4;
   }

   protected void cw() {
      this.a *= 0.9F;
   }

   protected void bG() {
      this.motY = (double)(0.42F + (float)this.cB() * 0.1F);
      this.ai = true;
   }

   protected void bI() {
      this.motY = (double)(0.22F + (float)this.cB() * 0.05F);
      this.ai = true;
   }

   public void e(float var1, float var2) {
   }

   protected boolean cx() {
      return true;
   }

   protected int cy() {
      return super.cy() + 2;
   }

   protected String cz() {
      return this.cB() > 1?"mob.magmacube.big":"mob.magmacube.small";
   }

   protected boolean cA() {
      return true;
   }
}
