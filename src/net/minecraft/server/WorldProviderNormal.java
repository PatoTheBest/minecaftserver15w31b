package net.minecraft.server;

import net.minecraft.server.WorldProvider;
import net.minecraft.server.class_aoz;

public class WorldProviderNormal extends WorldProvider {
   public class_aoz p() {
      return class_aoz.a;
   }

   public boolean c(int var1, int var2) {
      return !this.b.c(var1, var2);
   }
}
